package a32_stream_count_min_max;

import java.util.*;

public class StreamTest {

	public static void main(String[] args) {
		
		Locale[] locales = Locale.getAvailableLocales();
		
		a1(locales);
		a2(locales);
		a3(locales);
	}
	
	static void a1(Locale[] locales) {
		Object max = Arrays.stream(locales)
			.map(x -> x.getDisplayCountry())
			.sorted().max(Comparator.naturalOrder()).get();

		System.out.println("Land mit dem lexikographisch größten Wert: "+ max);
	}
	
	static void a2(Locale[] locales) {
		long anz = Arrays.stream(locales)
			.filter(x -> x.getLanguage().equals("de"))
			.count();
		System.out.println("Anzahl der Länder mit Sprache \"de\": " + anz);
	}
	
	static void a3(Locale[] locales) {
		//A
//		List<Locale> filtered = new ArrayList<>();
//		for (Locale locale : locales) {
//			if (locale.getDisplayCountry().contains("t")) {
//				filtered.add(locale);
//			}
//		}
//		
//		Comparator<Locale> cmp = (loc1, loc2) -> loc1.getDisplayLanguage().compareTo(loc2.getDisplayLanguage());
//		
//		filtered.sort(cmp);
//		
//		if (filtered.size() > 0) {
//			Locale min = filtered.get(0);
//			System.out.println(min.getDisplayCountry());
//			System.out.println(min.getDisplayLanguage());
//		}
		//B
		
		String min = Arrays.stream(locales)
			.filter(x -> x.getDisplayCountry().contains("t"))
			.min((loc1, loc2) -> loc1.getDisplayLanguage().compareTo(loc2.getDisplayLanguage()))
			.map(x -> x.getDisplayCountry()+"\n"+x.getDisplayLanguage()).get();
		
		System.out.println("Land: "+min);
	}
}
