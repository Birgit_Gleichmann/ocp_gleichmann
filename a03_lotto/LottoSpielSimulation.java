package a03_lotto;

import java.util.*;

public class LottoSpielSimulation {

	public static void main(String[] args) {
		int anzahlKugeln = 7;
		int anzahlKugelnGesamt = 49;
		
		LottoSpiel lotto = new LottoSpiel(anzahlKugeln, anzahlKugelnGesamt);
		lotto.ziehen();
		System.out.println(lotto);
		
		LottoTipp tipp = new LottoTipp(anzahlKugeln, anzahlKugelnGesamt);
		tipp.abgeben();
		System.out.println(tipp);
		
		int gewinn = lotto.vergleichen(tipp);
		System.out.println("Gewinn: "+gewinn+"€");
		
		System.out.println("*****");
		gewinn = 0;
		int anzSpiele = 100000;
		for (int i= 0; i<anzSpiele; i++) {
			lotto.ziehen();
			gewinn += lotto.vergleichen(tipp);
		}
		gewinn -= anzSpiele;
		System.out.println("Gesamtgewinn: "+gewinn+"€");
	}
}

class LottoSpiel {
	int anzahlKugeln;
	int anzahlKugelnGesamt;
	Set<Integer> lottoZahlen;
	
	public LottoSpiel(int anzahlKugeln, int anzahlKugelnGesamt) {
		this.anzahlKugeln = anzahlKugeln;
		this.anzahlKugelnGesamt = anzahlKugelnGesamt;
	}
	
	public void ziehen() {
		lottoZahlen = new TreeSet<>();
		Random r = new Random();
		while(lottoZahlen.size()<anzahlKugeln) {
			lottoZahlen.add((r.nextInt(anzahlKugelnGesamt)+1));
		}
	}
	
	public int vergleichen(LottoTipp tipp) {
		int gewinn = 0;
		
		int counter = 0;
		
		Iterator it = tipp.tippZahlen.iterator();    
		while (it.hasNext()) { 
			if (lottoZahlen.contains((Integer) it.next())) {
				counter++;
			}
		}
		gewinn = (int)(Math.pow(10, counter-1));
		return gewinn;
	}
	
	public String toString() {
		return "Spiel "+anzahlKugeln+" aus "+anzahlKugelnGesamt+". "+lottoZahlen;
	}
}

class LottoTipp {
	int anzahlKugeln;
	int anzahlKugelnGesamt;
	Set<Integer> tippZahlen = new TreeSet<>();
	
	public LottoTipp(int anzahlKugeln, int anzahlKugelnGesamt) {
		this.anzahlKugeln = anzahlKugeln;
		this.anzahlKugelnGesamt = anzahlKugelnGesamt;
	}
	
	public void randomAbgeben() {
		Random r = new Random();
		while(tippZahlen.size()<anzahlKugeln) {
			tippZahlen.add((r.nextInt(anzahlKugelnGesamt)+1));
		}
	}
	
	public void einlesen() {
        int zahl = 0;
		try (Scanner myScanner = new Scanner(System.in)) {
            while (tippZahlen.size()<anzahlKugeln) {
                try {
                    System.out.print("Geben Sie eine Zahl ein zwischen 1 und "+anzahlKugelnGesamt+": ");
                    zahl = myScanner.nextInt();
                    if ((zahl < 1) || (zahl > anzahlKugelnGesamt)) {
                        System.out.println("Zahl ausserhalb des Bereichs");
                    } else {
                        tippZahlen.add(zahl);
                    }
                } catch (InputMismatchException e) {
                    System.out.println("Keine gültige Zahl");
                    myScanner.next();
                }
            }	
		}
	}
	
	public void abgeben() {
        int choice = 0;
		try (Scanner myScanner = new Scanner(System.in)) {
			try {
				System.out.print("Wollen Sie die "+anzahlKugeln+" Zahlen eingeben oder zufällig erzeugeln lassen? \n1. Eingeben\n2. Zufällig erzeugen\nBeenden\nAuswahl:");
                choice = myScanner.nextInt();
                if (choice == 1) {
                	einlesen();
                } else if (choice == 2){
                	randomAbgeben();
                } else {
                 	System.out.println("Programm wird beendet ...");
                	System.exit(0);
                }
            } catch (InputMismatchException e) {
             	System.out.println("Programm wird beendet ...");
               	System.exit(0);
            }
		}
	}
	
	public String toString() {
		return "Tipp "+anzahlKugeln+" aus "+anzahlKugelnGesamt+". "+tippZahlen;
	}
}
