package a18_set_fileTypes;

import java.io.*;
import java.util.*;

public class FileTypes {

	File file;
	
	public FileTypes(String s) {
		file = new File(s);
		
		System.out.println(file);
	}
	
	public Collection<String> getFileTypes() {
		Collection<String> coll = new TreeSet<>();
		File[] files = file.listFiles();
		String name = null;
		int index = -1;
		String ext = null;
		try {
			for(File f : files) {
				if (f.isDirectory()) {
					continue;
				}
				name = f.getName();
				index = name.lastIndexOf(".");
				if (index < name.length()) {
					ext = name.substring(index+1);
					coll.add(ext);
				}
			}
		} catch (Exception e) {
			System.out.println("Fehler");
		}
		return coll;
	}
}
