package a18_set_fileTypes;

import java.util.*;

public class TestFileTypes {

	public static void main(String[] args) {

		FileTypes ft = new FileTypes("C:\\Windows");
		Collection<String> extColl = ft.getFileTypes();
		System.out.println(extColl);
		
		System.out.println("*****");
		ft = new FileTypes("Hallo");
		extColl = ft.getFileTypes();
		
	}

}
