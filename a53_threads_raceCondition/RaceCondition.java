package a53_treads_raceCondition;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

class RaceThread extends Thread {
	@Override
	public void run() {
		
		while (true) {
			System.out.println("Race");
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.exit(0);
			}
		}
	}
}

public class RaceCondition {

	static volatile int count;

	public static void main(String[] args) {
		
//		a1();
//		a2();
//		a3();
		a4();
	}
	
	static synchronized void increment() {
		for (int i=0; i<1_000_000; i++) {
			count++;
		}
	}
	static synchronized void decrement() {
		for (int i=0; i<1_000_000; i++) {
			count--;
		}
	}
	
	static void a4() {
		
		Thread thA = new Thread() {
			@Override
			public void run() {
				increment();
			}
		};
		Thread thB = new Thread() {
			@Override
			public void run() {
				decrement();
			}
		};
		
		thA.start();
		thB.start();
		
		try {
			thA.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		try {
			thB.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Count: "+count);
	}
	
	static void a3() {
		
		Runnable target = () -> {
								for (int i=0; i<1000000; i++) {
									count++;
								}
		};
		Thread th = new Thread(target);
		th.start();
		
		try {
			th.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println(count);
	}
	
	static void a2() {
		
		ArrayList<Integer> aList = new ArrayList<>();
		ArrayList<Integer> bList = new ArrayList<>();
		
		Runnable targetA = () -> {
			int maxValue = 20;
			Random r = new Random();
			for (int i = 0; i < maxValue; i++) {
				aList.add(r.nextInt(101));
			}
		};

		Runnable targetB = () -> {
			int maxValue = 20;
			Random r = new Random();
			for (int i = 0; i < maxValue; i++) {
				bList.add(r.nextInt(101));
			}
		};
		
		Thread a = new Thread(targetA);
		Thread b = new Thread(targetB);
		
		a.start();
		b.start();
		
		try {
			a.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		try {
			b.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		int sum = Stream.concat(aList.stream(), bList.stream())
			.reduce(0, (x,y) -> x+y)
		;
		
		System.out.println("Summe: "+sum);
	}
	
	static void a1() {

		Thread th = new RaceThread();
		th.start();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		th.interrupt();
	}
}
