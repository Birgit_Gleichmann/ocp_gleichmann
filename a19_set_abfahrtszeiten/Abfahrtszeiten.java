package a19_set_abfahrtszeiten;

import java.util.*;
import java.time.*;

public class Abfahrtszeiten {

	private static TreeSet<String> generateTree() {
		TreeSet<String> tree = new TreeSet<>();
		String time = "";
		String hour = "";
		for (int i=6, j=1; i<24; i++) {
			hour =  String.format("%02d", i);
			for (j=0; j<3; j++) {
				time = hour+":"+ (12+j*20);
				tree.add(time);
			}
		}
		System.out.println(tree);
		return tree;
	}
	
	private static TreeSet<LocalTime> generateTimeTree() {
		TreeSet<LocalTime> tree = new TreeSet<>();
		LocalTime time = LocalTime.of(6, 12);
		for (int i=0; i<(23-5)*3; i++) {
			tree.add(time);
			time = time.plusMinutes(20);
		}
		System.out.println(tree);
		return tree;
	}

	public static void main(String[] args) {
		TreeSet<String> tree = generateTree();
		
		System.out.println("*****");
		
		System.out.println(tree.higher("12:03"));
		System.out.println(tree.lower("12:03"));
		System.out.println(tree.ceiling("17:12"));
		System.out.println(tree.higher("17:12"));
		System.out.println(tree.subSet("12:00", false, "13:00", false));
		System.out.println(tree.subSet("11:52", false, "13:12", true));
		System.out.println(tree.first());
		System.out.println(tree.last());
		
		System.out.println("*****");
		
		TreeSet<LocalTime> timeTree = generateTimeTree();
		System.out.println(timeTree.higher(LocalTime.of(12, 3)));
		System.out.println(timeTree.lower(LocalTime.of(12, 3)));
		System.out.println(timeTree.ceiling(LocalTime.of(17, 12)));
		System.out.println(timeTree.higher(LocalTime.of(17, 12)));
		System.out.println(timeTree.subSet(LocalTime.of(12, 0), false, LocalTime.of(13, 0), false));
		System.out.println(timeTree.subSet(LocalTime.of(11, 52), false, LocalTime.of(13, 12), true));
		System.out.println(timeTree.first());
		System.out.println(timeTree.last());
	}

}
