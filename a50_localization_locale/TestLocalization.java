package a50_localization_locale;

import java.time.*;
import java.time.format.*;
import java.util.*;

public class TestLocalization {
	
	public static void main(String[] args) {
		Locale[] allLocales = Locale.getAvailableLocales();
		
		LocalDateTime actualDate = LocalDateTime.now();
		DateTimeFormatter formatterNow = DateTimeFormatter.ofPattern("EEEE, dd. MMMM yyyy, HH:mm");
		System.out.println(actualDate.format(formatterNow));
		System.out.println("*****");
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEEE, dd. MMMM yyyy");
		System.out.println(actualDate.format(formatter));
		
		for (Locale l : allLocales) {
			System.out.println(actualDate.format(formatter.withLocale(l)));
		}
	}
}
