package a09_nested_comparator;

public class TestComparator {

	public static void main(String[] args) {

		String s1 = "Hallo";
		String s2 = "Montag";
		
		System.out.println(MyComparator.compare1(s1, s2));
		System.out.println(MyComparator.compare2(s1, s2));
		System.out.println(MyComparator.compare3(s1, s2));
		System.out.println(MyComparator.compare4(s1, s2));
		System.out.println(MyComparator.compare5(s1, s2));
		System.out.println(MyComparator.compare6(s1, s2));
		
	}

}
