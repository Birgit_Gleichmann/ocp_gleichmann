package a09_nested_comparator;

import java.util.*;

public class MyComparator {

	static int compare1(String s1, String s2) {
		Comparator<String> c = new OuterComparator();
		return c.compare(s1, s2);
	}
	
	static int compare2(String s1, String s2) {
		Comparator<String> c = new MyComparator().new InnerComparator();
		return c.compare(s1, s2);
	}

	static int compare3(String s1, String s2) {
		class LocalComparator implements Comparator<String> {
			public int compare(String s1, String s2) {
				return s1.length()-s2.length();
			}		
		}
		
		Comparator<String> c = new LocalComparator();
		return c.compare(s1, s2);
	}

	static int compare4(String s1, String s2) {
		Comparator<String> c = new Comparator<String>() {
			public int compare(String s1, String s2) {
				return s1.length()-s2.length();
			}		
		};
		return c.compare(s1, s2);
	}

	static int compare5(String s1, String s2) {
		Comparator<String> c = (x, y) -> x.length()-y.length();
		return c.compare(s1, s2);
	}

	static int compare6(String s1, String s2) {
		Comparator<String> c = MyComparator::compare5;
		return c.compare(s1, s2);
	}
	
	class InnerComparator implements Comparator<String> {
		public int compare(String s1, String s2) {
			return s1.length()-s2.length();
		}		
	}
}

class OuterComparator implements Comparator<String> {
	public int compare(String s1, String s2) {
		return s1.length()-s2.length();
	}
}
