package a24_map_besitzer_fahrzeuge;

import java.util.*;

public class MapTest {

	public static void main(String[] args) {

		// *** A1
		Person p1 = new Person("Hans", "Müller");
		Person p2 = new Person("Inge", "Meier");
		Person p3 = new Person("Susi", "Schmidt");
		
		Person p4 = new Person("Konrad", "Hase");

		Fahrzeug f1 = new Fahrzeug("Golf", "VW");
		Fahrzeug f2 = new Fahrzeug("Fox", "VW");
		Fahrzeug f3 = new Fahrzeug("Corsa", "Opel");
		Fahrzeug f4 = new Fahrzeug("Ibiza", "Seat");
		
		Fahrzeug f5 = new Fahrzeug("Astra", "Opel");
		
		// *** A2
		HashMap<Fahrzeug, Person> mapKFZ = new HashMap<>();
		mapKFZ.put(f1, p2);
		mapKFZ.put(f2, p3);
		mapKFZ.put(f3, p1);
		mapKFZ.put(f4, p2);
		
		System.out.println(mapKFZ);
		System.out.println("*****");
		printMap(mapKFZ);
		
		System.out.println("*****");
		System.out.println(mapKFZ.get(f2));
		System.out.println(mapKFZ.get(f5));
		
		System.out.println("*****");
		ArrayList<Fahrzeug> af1 = new ArrayList<>();
		af1.add(f4);
		af1.add(f1);
		ArrayList<Fahrzeug> af2 = new ArrayList<>();
		af2.add(f3);
		ArrayList<Fahrzeug> af3 = new ArrayList<>();
		af3.add(f2);
		
		HashMap<Person, ArrayList<Fahrzeug>> mapPerson = new HashMap<>();
		
		mapPerson.put(p1, af1);
		mapPerson.put(p2, af2);
		mapPerson.put(p3, af3);
		
		printMap(mapPerson);

		System.out.println("*****");
		System.out.println(mapPerson.get(p1));
		System.out.println(mapPerson.get(p4));
	}
	
	static void printMap(HashMap m) {

		m.keySet().forEach(x -> System.out.println(x + " -> " + m.get(x)));
	}

}

class Fahrzeug {
	String modell;
	String hersteller;
	
	public Fahrzeug(String modell, String hersteller) {
		this.modell = modell;
		this.hersteller = hersteller;
	}
	
	@Override
	public String toString() {
		return hersteller + " " + modell;
	}
}

class Person {
	String vorname;
	String nachname;
	
	public Person(String vorname, String nachname) {
		this.vorname = vorname;
		this.nachname = nachname;
	}

	@Override
	public String toString() {
		return vorname + " " + nachname;
	}
}