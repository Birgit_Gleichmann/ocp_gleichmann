package a22_deque_mirror;

import java.util.*;

public class Mirror {

	Deque<String> text = new ArrayDeque<>();
	
	public void add(char c) {
		if (text.isEmpty())
			text.offer(" | ");
		text.offerFirst(c+"");
		text.offerLast(c+"");
	}
	
	public void remove( ) {
		text.pollFirst();
		text.pollLast();
		if (text.peek().equals(" | ")) 
			text.poll();
		
	}
	
	public boolean isEmpty() {
		return text.isEmpty();
	}
	
	@Override
	public String toString() {
		String retString = "";
		for (String s:text)
			retString += s;
		return retString;
	}
}
