package a22_deque_mirror;

public class MirrorTest {

	public static void main(String[] args) {

		Mirror m = new Mirror();
		
		for (char ch='a'; ch<'g'; ch++) {
			m.add(ch);
			System.out.println(m);
		}
		
		while ( !m.isEmpty()) {
			System.out.println(m);
			m.remove();
		}
		
	}

}
