package a54_threads_printer;

class PrinterRunnable implements Runnable {
	
	char c;
	int col;
	
	public PrinterRunnable(char c, int col) {
		this.c = c;
		this.col = col;
	}
	
	@Override
	public void run() {
		synchronized( PrinterRunnable.class ) { 
			for (int i = 0; i < col; i++) {
				System.out.print(c);;
			}
			System.out.println();
		}
	}
}

public class Printer {
	
	char c;
	int col;
	int row;
	
	Thread printerThread;
	
	public Printer(char c, int col, int row) {
		this.c = c;
		this.col = col;
		this.row = row;
		
		
	}
	
	public void start() {
		
		Runnable rPrinter = () -> {
			for (int i=0; i<row; i++) {
				Runnable r = new PrinterRunnable(c, col);
				Thread t = new Thread(r);
				t.start();
			}
		};
		Thread tPrinter = new Thread(rPrinter);
		tPrinter.start();
	}
}
