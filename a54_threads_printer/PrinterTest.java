package a54_threads_printer;

public class PrinterTest {

	public static void main(String[] args) {
		
		Printer p1 = new Printer('a', 10, 20);
		p1.start();
		
		Printer p2 = new Printer('*', 15, 40);
		p2.start();
	}
}
