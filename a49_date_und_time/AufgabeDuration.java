package a49_date_und_time;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class AufgabeDuration {

	public static void main(String[] args) {

		// Reise-Start:  17:00 Uhr, UTC+7
		// Reise-Ende: 08:00 Uhr, UTC+2, aber am nächsten Tag
		
		// Duration?
		
		int year = 2000;
		int month = 1;
		int day = 1;
		int hour = 17;
		int minute = 0;
		int second = 0;
		int nano = 0;
		ZoneId zone1 = ZoneId.of("UTC+7");
		
		ZonedDateTime punkt1 = ZonedDateTime.of(year, month, day, hour, minute, second, nano, zone1);
		
		ZonedDateTime punkt2 = ZonedDateTime.of(year, month, day+1, 
				8, minute, second, nano, ZoneId.of("UTC+2"));
		
		Duration duration1 = Duration.between(punkt1, punkt2);
		System.out.println(duration1);

	}

}
