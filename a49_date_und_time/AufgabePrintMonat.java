package a49_date_und_time;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class AufgabePrintMonat {

	public static void main(String[] args) {

		Locale.setDefault(Locale.ENGLISH);
		printMonat(2, 2010);
	}
	
	public static void printMonat(int month, int year) {
		
		LocalDate firstDayOfTheMonth = LocalDate.of(year, month, 1);
		int daysOfMonth = firstDayOfTheMonth.lengthOfMonth();
		
		LocalDate daysOfTheMonth = LocalDate.of(year, month, 1);
		String dayPattern = "EEEE";
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(dayPattern);
		
		
		System.out.println("---------------------");
		DateTimeFormatter monthFormatter = DateTimeFormatter.ofPattern("MMMM yyyy");
		System.out.println(monthFormatter.format(daysOfTheMonth));
		
		
		System.out.println("---------------------");
		
		for (int i = 1; i<=daysOfMonth; i++) {
			daysOfTheMonth = LocalDate.of(year, month, i);
			String weekday = dateFormatter.format(daysOfTheMonth);
			System.out.printf("| %02d | %-12s |  %n",i, weekday);
		}
		System.out.println("---------------------");
		
		
		
		
		
	}

}
