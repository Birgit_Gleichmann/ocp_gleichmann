package a06_exceptions_parseDate;

import java.util.*;

import javax.swing.text.DateFormatter;

import java.time.*;
import java.time.format.*;

public class ParseDate {

	public static void main(String[] args) {

		LocalDate myDate = null;
		
		String dateAsString;
		boolean hasDate = false;
		while (!hasDate) {
			dateAsString = getUserInput();
			try {
				myDate = LocalDate.parse(dateAsString);
				hasDate = true;
			} catch (Exception e) {}
		}
		
		System.out.print("Datum: ");
		System.out.println(myDate);
		Locale locale = Locale.GERMAN;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd. MMMM yyyy", locale);
		String dateString = myDate.format(formatter);
		System.out.println(dateString);
		locale = Locale.ENGLISH;
		formatter = DateTimeFormatter.ofPattern("dd. MMMM yyyy", locale);
		dateString = myDate.format(formatter);
		System.out.println(dateString);
	}
	
	static private String getUserInput() {
        String myDate = null;
		System.out.print("Geben Sie ein Datum ein (JJJJ-MM-TT): ");
		myDate = new Scanner(System.in).next();
		return myDate;
	}

}
