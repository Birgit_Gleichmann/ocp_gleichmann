package a15_list_benchmark;

import java.util.*;

public class BenchmarkAddFirst {

	static List<String> stringList;
	
	public static void testAddFirst(String... strings) {
		stringList = new ArrayList<>();
		for (String s : strings) {
			stringList.add(0, s);
		}
	}

	public static void testAddFirstLinked(String... strings) {
		stringList = new LinkedList<>();
		for (String s : strings) {
			stringList.add(0, s);
		}
	}
	
	public static void main(String[] args) {
		
		int anz = 1000;
		int m = 20000;
		
		String[] strings = new String[anz];
		for(int i=0; i<anz; i++) {
			strings[i] = Integer.toString(i);
		}
		
		long start = System.currentTimeMillis();
		for (int i=0; i<m; i++) {
			testAddFirst(strings);
		}
		long end = System.currentTimeMillis();
		System.out.println("Zeit ArrayList: "+(end - start));

		start = System.currentTimeMillis();
		for (int i=0; i<m; i++) {
			testAddFirstLinked(strings);
		}
		end = System.currentTimeMillis();
		System.out.println("Zeit LinkedList: "+(end - start));
	}
}
