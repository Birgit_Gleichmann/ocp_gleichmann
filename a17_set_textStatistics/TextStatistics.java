package a17_set_textStatistics;

import java.util.*;

public class TextStatistics {
	
	static String text;
	
	static TextStatistics of(String s) {
		text = s;
		return new TextStatistics();
	}
	
	Collection<Character> getUniqueChars() {
		Set<Character> list = new HashSet<>();
		for (int i=0; i<text.length(); i++) {
			list.add(new Character(text.charAt(i)));
		}
		
		return list;
	}

	public static void main(String[] args) {
		TextStatistics stat = TextStatistics.of("Heute ist Montag!");
		Collection<Character> chars = stat.getUniqueChars();
		System.out.println(chars);
	}

}
