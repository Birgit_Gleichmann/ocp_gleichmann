# Aufgabe 'intermediate operations'

Gegeben sind folgende Listen, die die EMail-Adressen beinhalten:
	
		List<String> mailsErsthelfer = Arrays.asList("tom@mycompany.com", "jerry@mycompany.com");
		List<String> mailsIT = Arrays.asList("tom@mycompany.com", "mary@mycompany.com");
		List<String> mailsQM = Arrays.asList("peter@mycompany.com", "jerry@mycompany.com");

Die Listen werden in eine Pipeline gesammelt:

		Stream.of(mailsSupport, mailsIT, mailsQM)
			...
			forEach(System.out::println);

Gestalten Sie die Pipeline so, dass sie einzigartige Mitarbeiter-Namen ausgibt:

		tom
		jerry
		mary
		peter
