package a31_stream_intermediate;

import java.util.*;
import java.util.stream.*;

public class Test {

	public static void main(String[] args) {

		List<String> mailsSupport = 
				Arrays.asList("tom@mycompany.com", "jerry@myCompany.com");
		List<String> mailsIT = 
				Arrays.asList("tom@mycompany.com", "mary@myCompany.com");
		List<String> mailsQM = 
				Arrays.asList("peter@mycompany.com", "jerry@myCompany.com");
		
		Stream.of(mailsSupport, mailsIT, mailsQM)
			.flatMap(x -> x.stream()).distinct().map(x -> x.substring(0, x.indexOf("@")))
			.forEach(System.out::println);
	}

}
