package a01_interfaces;

import java.util.Arrays;
import java.util.Comparator;

class Person implements Comparable<Person>{
	String vorname;
	String nachname;
	int geburtsjahr;
	
	Person(String vorname, String nachname, int geburtsjahr) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsjahr = geburtsjahr;
	}
	
	@Override
	public String toString() {
		return vorname + " " + nachname + ", "+geburtsjahr;
	}
	
	@Override
	public int compareTo(Person p2) {
		int result = nachname.compareTo(p2.nachname); // compareTo der Klasse String
		
		if(result==0) {
			result = vorname.compareTo(p2.vorname);
			
			if(result==0) {
				result = geburtsjahr - p2.geburtsjahr;
			}
		}
		
		return result;
	}
}

class ComparatorReverse implements Comparator<Person> {
	@Override
	public int compare(Person p1, Person p2) {
		return p2.compareTo(p1);
	}
}

public class A01_Interfaces {
	
	static Person[] personenArray;

	public static void main(String[] args) {
		
		initNamesArray();
		
		namenSortieren();
		reverseSort();
		
	}
	
	private static void initNamesArray() {	
		personenArray =  new Person[]{
				new Person("Paul", "Smith", 1999),
				new Person("Paul", "Black", 2002),
				new Person("John", "Smith", 1981),
				new Person("Paul", "Black", 1991),
				new Person("John", "Black", 2005),
				new Person("John", "Smith", 2001)};
		
		for (Person person : personenArray)
			System.out.println(person);
	}
	
	private static void namenSortieren() {

		System.out.println("***** aufsteigend sortieren");
	
		Arrays.sort(personenArray);
		for (Person person : personenArray)
			System.out.println(person);
		
		int pos = Arrays.binarySearch(personenArray, new Person("John", "Black", 2005));
		System.out.println("Position: "+pos);
		
	}
	
	private static void reverseSort() {
			
		System.out.println("***** absteigend sortieren");
		
		Comparator<Person> cmp = new ComparatorReverse();
		Arrays.sort(personenArray, cmp);
		for (Person person : personenArray)
			System.out.println(person);
		
		int pos = Arrays.binarySearch(personenArray,new Person("John", "Smith", 1981) , cmp);
		System.out.println("Position: "+pos);
	}
}
