package a36_maps_functional;

import java.util.*;
import java.util.stream.*;
import java.util.function.*;

public class MapTest {

	public static void main(String[] args) {
		
		System.out.println("*****");
		
		// A1
		Map<Integer, String> map1 = createMap();
		BiConsumer<? super Integer, ? super String> action = (x, y) -> System.out.println(x + " = " + y);
		map1.forEach(action );
		
		// A2
		Map<Integer, String> map2 = createMap();
		BiFunction<? super Integer, ? super String, ? extends String> remappingFunction = (x, y) -> "Dienstag";
		map2.compute(2, remappingFunction);
		System.out.println(map2);
		
		// A3
		Map<Integer, String> map3 = createMap();
		map3.computeIfAbsent(2, k -> "Dienstag");
		System.out.println(map3);
		
		// A4
		Map<Integer, String> map4 = createMap();
		map4.computeIfPresent(2, (k,v) -> "Dienstag");
		System.out.println(map4);
		
		// A5
		Map<Integer, String> map5 = createMap();
		
		BiFunction<? super String, ? super String, ? extends String> emappingFunction1 = 
				(existingValue, additionalValue) -> {System.out.println("existing: "+existingValue);
													System.out.println("additional: "+additionalValue);
													return existingValue + " (" + additionalValue + ")";
				}
		;
		map5.merge(2, "Dienstag", emappingFunction1);

		BiFunction<? super String, ? super String, ? extends String> emappingFunction2 = 
				(existingValue, additionalValue) -> {System.out.println("existing: "+existingValue);
													System.out.println("additional: "+additionalValue);
													return existingValue + " (" + additionalValue + ")";
				}
		;
		map5.merge(4, "Donnestag", emappingFunction2);
		
		map5.forEach((i, v) -> System.out.println(i + " = " + v));
		
	}
	
	static HashMap<Integer, String> createMap() {
		HashMap<Integer, String> map = new HashMap<>();

		map.put(1, "Mo");
		map.put(2, "Di");
		map.put(3, "Mi");
		
		return map;
	}
	
	
	
}
