# Aufgabe 'Maps - functional'

> Die Schreibweise `TypA#method(TypB)` soll im folgenden eine vereinfachte Beschreibung einer Instanzmethode darstellen. So wird mit `Map#forEach(BiConsumer)` die Methode `forEach` aus dem Interface `Map` gemeint, die einen `BiConsumer`-Parameter  dekalriert.


#### A1.
Geben Sie die Inhalte einer Map mit der Methode `Map#forEach(BiConsumer)` auf der Console untereinander aus. 

#### A2.
Gegeben ist eine Integer-to-String-Map mit folgenden Inhalten:
   
	1 = Mo
	2 = Di
	3 = Mi

Verwenden Sie bitte die Methode `Map#compute(BiFunction)` um den Wert "Di" durch den Wert "Dienstag" zu ersetzen. 

#### A3.
Gegeben ist eine Integer-to-String-Map namens `map` mit folgenden Inhalten:
   
	1 = Mo
	2 = Di
	3 = Mi
	
Wie sehen die Inhalte der Map aus nach folgendem Aufruf?
   
	map.computeIfAbsent(2, k -> "Dienstag");
	
#### A4.
Gegeben ist eine Integer-to-String-Map namens `map` mit folgenden Inhalten:
   
	1 = Mo
	2 = Di
	3 = Mi
	
Wie sehen die Inhalte der Map aus nach folgendem Aufruf?
   
	map.computeIfPresent(2, (k,v) -> "Dienstag");
	
#### A5. Optional
Gegeben ist eine Integer-to-String-Map namens `map` mit folgenden Inhalten:
   
	1 = Mo
	2 = Di
	3 = Mi
	
Was ist das Ergebnis aus dem folgenden Code?
	
		map.merge(2, "Dienstag", (existingValue, additionalValue) -> { 
			System.out.println("existing: " + existingValue);
			System.out.println("additiona: " + additionalValue);
			return existingValue + " (" + additionalValue + ")";
		} );
		
		map.merge(4, "Do", (existingValue, additionalValue) -> { 
			System.out.println("existing: " + existingValue);
			System.out.println("additiona: " + additionalValue);
			return existingValue + " (" + additionalValue + ")";
		} );
		
		map.forEach( (i,v) -> System.out.println( i + " = " + v ) );	

