package a48_streams_and_files;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import words.res.ResourceLoader;
import java.util.stream.*;
import java.util.function.*;

public class AufgabeStreamsAndFiles {

	public static void main(String[] args) {

		// A1.
		// Bitte in einem eigenen Projekt den Zugriff auf die Klasse
		// ResourceLoader testen
		
		List<String> words = ResourceLoader.englishWords();
		
		//
		// verwenden Sie bitte im weiteren die Liste words als Datenquelle
		// 
		
		// A2.
		// Definieren Sie bitte eine Pipeline, die die Anzahl der 
		// Wörter der Länge 5 ermittelt 
		
		long count = words.stream().filter(w -> w.length()==5).count();
		System.out.println("count: " + count);
	
		// A3.
		/*

		Erzeugen Sie bitte folgende Dateien auf der Festplatte in 
		einem Verzeichnis ihrer Wahl:
	
		mydir/
			|- a.txt
			|- b.txt
			...
			|- z.txt

		Jede von Ihnen erzeugte Datei beinhaltet alle Wörter mit dem 
		entsprechenden Anfangsbuchstaben ('a.txt' hat alle Wörter auf 'a', 
		 'b.txt' alle Wörter auf 'b' usw.)   

		 */
		
		Path path = Paths.get("englWords");
		try {
			Files.createDirectories(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
//		String pathString = "";
//		
//		for (char c='a'; c<='z'; c++) {
//			String ch = c+"";
//			pathString = path.toString()+"/"+c+".txt";
//			try(PrintWriter out = new PrintWriter(pathString)) {
//				words.stream().filter(w -> w.toLowerCase().startsWith(new String(ch))).forEach(x -> out.println(x));
//			} catch (FileNotFoundException e) {
//				e.printStackTrace();
//			}
//		}
		
		Function<String, Character> classifier = s -> s.charAt(0);
		
		Collector<String, ?, Map<Character, List<String>>> collector = Collectors.groupingBy(classifier);
		
		Map<Character, List<String>> groups = words.stream().collect(collector);
		
		for (Character ch : groups.keySet()) {
			List<String> group = groups.get(ch);
			Path file = path.resolve(ch + ".txt");
			try {
				Files.write(file, group);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

}
