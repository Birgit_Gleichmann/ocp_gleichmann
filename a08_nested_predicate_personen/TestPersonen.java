package a08_nested_predicate_personen;

import java.util.*;
import java.util.function.*;

public class TestPersonen {

	public static void main(String[] args) {

		class PrivatePredicate implements Predicate<Person> {
			public boolean test(Person p) {
				if (p.nachname.length() >= 6) return true;
				else return false;
			}
		}

		Person[] personen = {
				new Person("Klaus", "Meier", 1980),
				new Person("Lisa", "Schmidt", 1985),
				new Person("Susi", "Esser", 1982),
				new Person("Hugo", "Schneider", 1988),
				new Person("Jens", "Klaas", 2001),
				new Person("Tom", "Maier", 1982),
				new Person("Gustav", "Tom", 1990),
				new Person("Rudi", "Rommel", 1982)
		};
		

		
		// A1 

		Predicate<Person> outerPredicate = new OuterPredicate();
		List<Person> personenList = filtern(personen, outerPredicate);
		System.out.println(personenList);

		//A2
		
		Predicate<Person> innerPredicate = new TestPersonen().new InnerPredicate();
		personenList = filtern(personen, innerPredicate);
		System.out.println(personenList);
		
		// A3
		
		Predicate<Person> privatePredicate = new PrivatePredicate();
		personenList = filtern(personen, privatePredicate);
		System.out.println(personenList);
		
		// A4
		
		Predicate<Person> anonymousPredicate = new Predicate<Person>() {
			public boolean test(Person p) {
				Predicate p1 = new OuterPredicate();
				Predicate p2 = new TestPersonen().new InnerPredicate();
				
				return (p1.test(p) && p2.test(p));
			}
		};
		personenList = filtern(personen, anonymousPredicate);
		System.out.println(personenList);
		
		// A5
		
		Predicate<Person> lambdaPredicate = x -> x.geburtsjahr%4 == 0;
		personenList = filtern(personen, lambdaPredicate);
		System.out.println(personenList);
		
	}
	
	static List<Person> filtern(Person[] personen, Predicate<Person> predicate) {
		
		List<Person> list = new ArrayList<>();
		for (int i=0; i<personen.length; i++) {
			if (predicate.test(personen[i])) {
				list.add(personen[i]);
			}
		}
		return list;
	}
	
	class InnerPredicate implements Predicate<Person> {
		public boolean test(Person p) {
			if (p.nachname.contains("a")) return true;
			else return false;
		}
	}
}


class Person {
	String vorname;
	String nachname;
	int geburtsjahr;
	
	public Person(String vorname, String nachname, int geburtsjahr) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsjahr = geburtsjahr;
	}
	
	public String toString() {
		return vorname + " " + nachname + ", " + geburtsjahr;
	}
}

class OuterPredicate implements Predicate<Person> {
	public boolean test(Person p) {
		if (p.geburtsjahr == 1982) return true;
		else return false;
	}
}