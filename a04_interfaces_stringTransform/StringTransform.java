package a04_interfaces_stringTransform;

import java.util.*;

public class StringTransform {

	public static void main(String[] args) throws Exception{
		String[] arr = {"mo", "di", "mit"};
		ArrayList<String> list = transform(arr);
		System.out.println(list);
		
		Command c1 = (String s)-> s+".";
        list = transform(arr, c1);
		System.out.println(list);

		Command c2 = (String s)-> s+"("+((Integer)(s.length())).toString()+")";
        list = transform(arr, c2);
		System.out.println(list);
		
		Command c3 = (String s)-> s.toUpperCase();
        list = transform(arr, c3);
		System.out.println(list);
		
		Command c4 = (String s)-> s+s;
		HashSet<String> hash = new HashSet<>(); 
		transform(arr, c4, hash);
		System.out.println(hash);
	}

	static ArrayList<String> transform(String[] stringArray) {
		ArrayList<String> myArrayList = new ArrayList<>();
		for(int i=0; i<stringArray.length; i++) {
			myArrayList.add(stringArray[i].toUpperCase());
		}
		return myArrayList;
	}
	
	static ArrayList<String> transform(String[] stringArray, Command c1) {
		ArrayList<String> myArrayList = new ArrayList<>();
		
		for(int i=0; i<stringArray.length; i++) {
			myArrayList.add(executeMethod(c1, stringArray[i]));
		}
		
		return myArrayList;
	}

	static void transform(String[] stringArray, Command c1, Collection<String> target) {
		for(int i=0; i<stringArray.length; i++) {
			target.add(executeMethod(c1, stringArray[i]));
		}
	}
	

	public static String executeMethod(Command command, String s){
        return command.execute(s);
    }

	interface Command{
    	String execute(String s);
	}
}


