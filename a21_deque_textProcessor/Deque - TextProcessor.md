# Aufgabe 'TextProcessor - Deque'

> Setzten Sie bitte bei der Lösung ein `Deque` ein.

###### A1.
Die Klasse `TextProcessor` erlaubt es die Strings zu speichern:

		TextProcessor p = new TextProcessor();
		p.add("a");
		p.add("b");
		p.add("c");
		
		System.out.println(p); // abc

###### A2.
Weiterhin ist die Klasse imstande ihre Zwischenzustände zu speichern und die Änderungen mit der Methode `undo` zurück zu setzen:

		System.out.println( p.undo() ); // true
		System.out.println( p ); // ab
		
		System.out.println( p.undo() ); // true
		System.out.println( p ); // a
		
		System.out.println( p.undo() ); // true
		System.out.println( p ); //
		
		System.out.println( p.undo() ); // false
		System.out.println( p ); // 


Die Methode `undo` liefert `true` zurück, falls es einen Zwischenzustand gab, zu dem der Textprocessor zurückgebracht wurde. Sie liefert `false` zurück, falls es keine gespeicherten älteren Zustände mehr gab.


###### A3.
Die Methode `redo` soll die am Anfang mit der `add` erzeugten und danach mit der `undo` zurücksetzten Änderungen wiederherstellen können:   
 
		System.out.println( p.redo() ); // true
		System.out.println( p ); // a
		
		System.out.println( p.redo() ); // true
		System.out.println( p ); // ab
		
		System.out.println( p.redo() ); // true
		System.out.println( p ); // abc
		
		System.out.println( p.redo() ); // false
		System.out.println( p ); // abc

Die Methode `redo` liefert `true` zurück, falls sie eine weitere `undo`-Änderung im TextProcessor zurücksetzen konnte. Sie liefert `false`, falls keine weitere `undo` Änderung mehr gibt.  


###### A4. Optional. 
Überlegen Sie, ob Sie für die Lösung das Entwurfsmuster 'Memento' einsetzen würden.
