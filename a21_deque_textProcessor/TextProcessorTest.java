package a21_deque_textProcessor;

public class TextProcessorTest {

	public static void main(String[] args) {

		TextProcessor p = new TextProcessor();
		p.add("a");
		p.add("b");
		p.add("c");
		
		System.out.println(p);
		
		System.out.println(p.undo());
		System.out.println(p);
		
		System.out.println(p.undo());
		System.out.println(p);
		
		System.out.println(p.undo());
		System.out.println(p);
		
		System.out.println(p.undo());
		System.out.println(p);
		
		System.out.println(p.redo());
		System.out.println(p);
		
		System.out.println(p.redo());
		System.out.println(p);
		
		System.out.println(p.redo());
		System.out.println(p);
		
		System.out.println(p.redo());
		System.out.println(p);
	}

}
