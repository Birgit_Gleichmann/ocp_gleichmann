package a21_deque_textProcessor;

import java.util.*;

public class TextProcessor {
	
	Deque<String> undoQueue = new ArrayDeque<>();
	Deque<String> textQueue = new ArrayDeque<>();
	
	public void add(String s) {
		textQueue.offer(s);
	}
	
	public boolean undo() {
		
		boolean retValue = false;
		String s = textQueue.pollLast();
		if (s != null) {
			undoQueue.offer(s);
			retValue = true;
		}
		return retValue;
	}
	
	public boolean redo() {
		boolean retValue = false;
		String s = undoQueue.pollLast();
		if (s != null) {
			textQueue.offer(s);
			retValue = true;
		}
		return retValue;
	}
	
	@Override
	public String toString() {
		String retString = "";
		for (String i : textQueue)
			retString += i + " ";
		return retString;
	}

}
