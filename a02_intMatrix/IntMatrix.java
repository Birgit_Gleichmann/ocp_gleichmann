package a02_intMatrix;

import java.util.*;

public class IntMatrix {

	int[][] matrix;
	
	public static void main(String[] args) {
		IntMatrix m1 = new IntMatrix(2, 3);
		IntMatrix m2 = new IntMatrix(5,3,100);
		
		System.out.println(m1);
		
		int i = m1.get(1, 2);
//		int j = m1.get(20, 10);
		
		System.out.println("*****");
		IntMatrix m3 = IntMatrix.getRandomMatrix(4, 6, 200);
		System.out.println(m3);
		
		IntMatrix m4 = new IntMatrix(5,6,100);
		System.out.println(m2.equals(m4));
		
		IntMatrix m5 = m3;
		System.out.println(m5.equals(m3));
		
		IntMatrix m6 = IntMatrix.getRandomMatrix(2, 3, 30);
		System.out.println(m6);
		IntMatrix m7 = m6.transponieren();
		System.out.println(m7);
		
		IntMatrix m8 = IntMatrix.getRandomMatrix(2, 3, 50);
		IntMatrix m9 = m8.add(m6);
		System.out.println(m8);
		System.out.println(m9);
		
		
	}

	public IntMatrix(int zeilen, int spalten, int initValue) {
		matrix = new int[zeilen][spalten];
		for (int[] i : matrix) {
			for (int j : i) {
				j=initValue;
			}
		}
	}
	
	public IntMatrix(int zeilen, int spalten) {
		matrix = new int[zeilen][spalten];
	}
	
	public static IntMatrix getRandomMatrix(int zeilen, int spalten,  int maxValue) {
		IntMatrix returnMatrix = new IntMatrix(zeilen, spalten);
		Random r = new Random();
		for (int i=0; i<returnMatrix.matrix.length; i++) {
			for(int j = 0; j< returnMatrix.matrix[i].length ; j++) {
				returnMatrix.matrix[i][j] = r.nextInt(maxValue+1);
			}
		}
		return returnMatrix;
	}
	
	public boolean equals (IntMatrix m2) {
		boolean returnValue = false;
		if (matrix.length == m2.matrix.length) {
			for (int i=0; i<matrix.length; i++) {
				if (matrix[i].length == m2.matrix[i].length) {
					for (int j=0; j<matrix[i].length; j++) {
						if (matrix[i][j] == m2.matrix[i][j]) {
							returnValue = true;
						}
						else {
							returnValue = false;
						}
					}
				}
				else {
					returnValue = false;
				}
			}
		}
		return returnValue;
	}
	
	public IntMatrix transponieren() {
		IntMatrix transpMatrix = null;
		if (matrix != null) {
			transpMatrix = new IntMatrix(matrix[0].length, matrix.length);
			for (int i=0; i<matrix.length; i++) {
				for (int j=0; j<matrix[i].length; j++) {
					transpMatrix.matrix[j][i] = matrix[i][j];
				}
			}
		}
		return transpMatrix;
	}
	
	public IntMatrix add(IntMatrix m2) {
		IntMatrix returnMatrix = m2;
		boolean fail = false;
		if (matrix.length == m2.matrix.length) {
			for (int i=0; i<matrix.length; i++) {
				if (matrix[i].length == m2.matrix[i].length) {
					for (int j=0; j<matrix[i].length; j++) {
						returnMatrix.matrix[i][j] = matrix[i][j] + m2.matrix[i][j]; 
					}
				}
				else {
					fail = true;
				}
			}
		}
		else {
			fail = true;
		}
		if (fail) {
			System.out.println("Matrizen haben unterschiedliche Dimensionen !");
			returnMatrix = null;
		}
			
		return returnMatrix;
	}
	
	public int get(int zeile, int spalte) {
		int returnValue = 0;
		try {
			returnValue = matrix[zeile][spalte];
		}
		catch (Exception e) {
			System.out.println(e);
		}
		return returnValue; 
	}
	
	public String toString() {
		String returnString = "";
		for (int[] i : matrix) {
			for (int j : i) {
				returnString += j+", \t";
			}
			returnString = returnString.substring(0, returnString.length()-3);
			returnString += "\n";
		}
			
		return returnString;
	}
}
