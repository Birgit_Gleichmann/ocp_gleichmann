package a35_stream_collect_numbers;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class CollectNumbersCollectors {
	public static void main(String[] args) {
		String[] arr = { 
				"1,2,3,4,5",
				"7,6,5,4,3",
				"123,456"
		};
		
		List<Integer> list;
		
		
		// A
//		list = new ArrayList<>();
//		for (String s : arr) {
//			String[] stringNumbers = s.split(",");
//			for (String sNum : stringNumbers) {
//				Integer num = Integer.valueOf(sNum);
//				list.add(num);
//			}
//		}
		// B
		
		Supplier<ArrayList<Integer>> collectionFactory = ArrayList::new;
		Collector<String, ?, ArrayList<Integer>> mapper 
				= Collectors.mapping(i -> Integer.valueOf(i), Collectors.toCollection(collectionFactory));

		BiConsumer<ArrayList<Integer>, ArrayList<Integer>> accumulator = (x, y) -> x.addAll(y);

		list = Arrays.stream(arr).map(s -> s.split(","))
			.map(x -> Arrays.stream(x).collect(mapper))
			.collect(collectionFactory, accumulator , accumulator)
		;

		System.out.println(list);
	}

}
