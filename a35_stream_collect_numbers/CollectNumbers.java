package a35_stream_collect_numbers;

import java.util.*;

public class CollectNumbers {
	
	public static void main(String[] args) {
		String[] arr = { 
				"1,2,3,4,5",
				"7,6,5,4,3",
				"123,456"
		};
		
		List<Integer> list;
		
		
		// A
		list = new ArrayList<>();
//		for (String s : arr) {
//			String[] stringNumbers = s.split(",");
//			for (String sNum : stringNumbers) {
//				Integer num = Integer.valueOf(sNum);
//				list.add(num);
//			}
//		}
		// B
		
		Arrays.stream(arr).map(x -> x.split(","))
		.forEach(stringNumbers -> Arrays.stream(stringNumbers)
									.map(sNum -> Integer.valueOf(sNum))
									.forEach(num -> list.add(num))
				);
		
		System.out.println(list);

		List<Integer> list2 = new ArrayList<>();
		Arrays.stream(arr).map(x -> x.split(","))
		.forEach(stringNumbers -> Arrays.stream(stringNumbers)
									.map(sNum -> Integer.valueOf(sNum)).filter(x -> x%2 == 0)
									.forEach(num -> list2.add(num))
				);
		
		System.out.println(list2);

	}

}
