package a33_stream_reduce_personen;

import java.util.*;
import java.util.function.*;

public class PersonenTest {

	public static void main(String[] args) {
		
		List<Person> personenList = new ArrayList<>();
		Person p1 = new Person("Tom", "Katze");
		Person p2 = new Person("Jerry", "Maus");
		Person p3 = new Person("Alexander", "Poe");
		
		personenList.add(p1);
		personenList.add(p2);
		personenList.add(p3);
		
		personenList.stream().forEach(System.out::println);

		System.out.println("*****");
		
		BinaryOperator<Person> accumulator = (x, y) -> x.max(y);
		Person identity = p1;
		BinaryOperator<Person> combiner = (x, y) -> x.max(y);
		
		System.out.println(personenList.stream().reduce(accumulator).get());
		System.out.println(personenList.stream().reduce(identity, accumulator));
		System.out.println(personenList.stream().reduce(identity, accumulator, combiner));
	}

}


class Person {
	String vorname;
	String nachname;
	
	public Person (String vorname, String nachname) {
		this.vorname = vorname;
		this.nachname = nachname;
	}
	
	public Person max(Person p2) {
		
		String vor = (vorname.compareTo(p2.vorname)>=0?vorname:p2.vorname);
		String nach = (nachname.compareTo(p2.nachname)>=0?nachname:p2.nachname);
		return new Person(vor, nach);
	}
	
	public String toString() {
		return vorname + " " + nachname;
	}
}