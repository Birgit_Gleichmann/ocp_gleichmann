package a44_files_dateienErstellen;

import java.io.*;
import java.nio.file.*;
import java.util.*;

public class FileTest {

	public static void main (String[] args) throws IOException {

		String root = "./myfiles";
		Files.createDirectories(Paths.get(root));
		createFiles(root, "file", "txt", 30);
		deleteFiles(root, "txt");
		
	}
	
	static void createFiles(String root, String prefix, String extension, int count) {
		Path path;
		for (int i=1; i<=count; i++) {
			String fileNameFormat = String.format(root+"/"+prefix+"%03d."+extension, i);
			
			path = Paths.get(fileNameFormat);
			try {
				Files.createFile(path);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	static void deleteFiles(String root, String extension) {
		Path path = Paths.get(root);
		
		try {
		Files.list(path).forEach(x -> {if (x.toString().endsWith("."+extension)) {
			try {Files.deleteIfExists(x);} catch (IOException e) {e.printStackTrace();}}});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

}
