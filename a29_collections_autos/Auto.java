package a29_collections_autos;

public abstract class Auto implements Comparable<Auto> {

	int baujahr;
	String modell;
	
	public Auto(String modell, int baujahr) {
		this.modell = modell;
		this.baujahr = baujahr;
	}
	
	@Override
	public int compareTo(Auto o) {
		int comp = modell.compareTo(o.modell);
		if (comp == 0) {
			comp = baujahr - o.baujahr;
		}
		return comp;
	}
	
	@Override
	public int hashCode() {
		return modell.hashCode();
	}
	
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + ". Modell: " + modell + ", Baujahr " + baujahr;
	}
}


