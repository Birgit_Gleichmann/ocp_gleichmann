package a29_collections_autos;

public class BMW extends Auto {

	public BMW (String modell, int baujahr) {
		super(modell, baujahr);
	}
	
	public void setBaujahr(int bj) {
		baujahr = bj;
	}
}
