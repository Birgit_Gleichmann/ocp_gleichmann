package a29_collections_autos;

import java.util.*;

public class TestAutos {

	public static void main(String[] args) {

		VW vw1 = new VW("Golf", 1990);
		BMW bmw1 = new BMW("Z4", 2000);
		
		System.out.println(vw1);
		System.out.println(bmw1);
		System.out.println();
		
		VW vw2 = new VW("Fox", 2002);
		VW vw3 = new VW("Touran", 2010);
		VW vw4 = new VW("Fox", 2000);
		
		VW vw5 = new VW("Polo", 2200);
		
		LinkedList<VW> vwLinkedList = new LinkedList<>();
		vwLinkedList.add(vw1);
		vwLinkedList.add(vw2);
		vwLinkedList.add(vw3);
		vwLinkedList.add(vw4);
		vwLinkedList.add(vw5);
			
		System.out.println(vwLinkedList);
		for (VW v : vwLinkedList)
			System.out.println(v);
		
		Collections.sort(vwLinkedList);
		System.out.println(vwLinkedList);
		int pos = Collections.binarySearch(vwLinkedList, new VW("Polo", 2200));
		System.out.println("Position: "+pos);
		pos = Collections.binarySearch(vwLinkedList, new VW("Polo", 3300));
		System.out.println("Position: "+pos);
		
		Comparator<VW> cmp = Comparator.reverseOrder(); 
		Collections.sort(vwLinkedList,cmp);
		System.out.println(vwLinkedList);
		pos = Collections.binarySearch(vwLinkedList, new VW("Polo", 2200));
		System.out.println("Position: "+pos);

		System.out.println();
		
		HashSet<VW> vwHashSet = new HashSet<>();
		vwHashSet.add(vw1);
		vwHashSet.add(vw2);
		vwHashSet.add(vw3);
		vwHashSet.add(vw4);
		
		System.out.println(vwHashSet);
		for (VW v : vwHashSet)
			System.out.println(v);
		System.out.println();
		
		TreeSet<VW> vwTreeSet = new TreeSet<>();
		vwTreeSet.add(vw1);
		vwTreeSet.add(vw2);
		vwTreeSet.add(vw3);
		vwTreeSet.add(vw4);
		
		System.out.println(vwTreeSet);
		for (VW v : vwTreeSet)
			System.out.println(v);
		System.out.println();
		
		PriorityQueue<VW> vwPriorityQueue = new PriorityQueue<>();
		vwPriorityQueue.add(vw1);
		vwPriorityQueue.add(vw2);
		vwPriorityQueue.add(vw3);
		vwPriorityQueue.add(vw4);
		
		System.out.println(vwPriorityQueue);
		for (VW v : vwPriorityQueue)
			System.out.println(v);
		System.out.println();

		BMW bmw2 = new BMW("i3", 2018);

		ArrayList<BMW> bmwArrayList = new ArrayList<>();
		bmwArrayList.add(bmw1);
		bmwArrayList.add(bmw2);
		
		System.out.println(bmwArrayList);
		for (BMW b : bmwArrayList)
			System.out.println(b);
		System.out.println();

		HashSet<BMW> bmwHashSet = new HashSet<>();
		bmwHashSet.add(bmw1);
		bmwHashSet.add(bmw2);
		
		System.out.println(bmwHashSet);
		for (BMW b : bmwHashSet)
			System.out.println(b);
		System.out.println();
		
		System.out.println("HashSet.contains BMW1: " + bmwHashSet.contains(bmw1));
		bmw1.setBaujahr(2004);
		System.out.println("Changed HashSet.contains BMW1: " + bmwHashSet.contains(bmw1));
		
		System.out.println();

		TreeSet<BMW> bmwTreeSet = new TreeSet<>();
		bmwTreeSet.add(bmw1);
		bmwTreeSet.add(bmw2);
		
		System.out.println(bmwTreeSet);
		for (BMW b : bmwTreeSet)
			System.out.println(b);
		System.out.println();

		ArrayDeque<BMW> bmwArrayDeque = new ArrayDeque<>();
		bmwArrayDeque.add(bmw1);
		bmwArrayDeque.add(bmw2);
		
		System.out.println(bmwArrayDeque);
		for (BMW b : bmwArrayDeque)
			System.out.println(b);
		System.out.println();

	}

}
