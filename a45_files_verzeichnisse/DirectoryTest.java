package a45_files_verzeichnisse;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.function.BiPredicate;

public class DirectoryTest {

	public static void main(String[] args) {

		Path path = Paths.get(".");
		printSubdirs(path);
		
		System.out.println("*****");
		printFiles(path);
		
		System.out.println("*****");
		createDirs("a\\b\\c\\d");

		System.out.println("*****");
		deleteDirs("a\\b\\c\\d");
	}

	public static void printSubdirs(Path path) {
		
		System.out.println("Unterverzeichnisse werden gesucht ...");
		try {
			Files.walk(path)
				.filter(x -> Files.isDirectory(x))
				.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void printFiles(Path path) {

		System.out.println("Dateien werden gesucht ...");

		BiPredicate<Path, BasicFileAttributes> matcher = (Path p, BasicFileAttributes atts) 
				-> {
					return Files.isRegularFile(p);
		};
				
		FileVisitOption[] options = {
				FileVisitOption.FOLLOW_LINKS
		};

				
		try {
			Files.find(path, 1, matcher, options)
				.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	public static void createDirs(String s) {
		Path path = Paths.get(s);
		try {
			System.out.println("Verzeichnisse werden erzeugt ...");
			Files.createDirectories(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void deleteDirs(String s) {
		Path path = Paths.get(s);
		try {
			Path parent = path;
			do {
				System.out.println("Lösche: " + parent);
				Files.delete(parent);
				parent = parent.getParent();
				
			} while(parent!=null);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
