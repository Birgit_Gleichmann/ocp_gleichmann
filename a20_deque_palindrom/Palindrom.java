package a20_deque_palindrom;

import java.util.*;
import java.util.stream.*;
import java.util.function.*;

public class Palindrom {

	public static void main(String[] args) {

		Character[] arr = {'a', 'n', 'n', 'a'};
		
		boolean erg = isPalindrom(arr);
		System.out.println(erg);

		arr = null;
		erg = isPalindrom(arr);
		System.out.println(erg);
		
		arr = new Character[] {'r', 'o', 't', 'o', 'r'};
		erg = isPalindrom(arr);
		System.out.println(erg);

		arr = new Character[] {'m', 'o', 't', 'o', 'r'};
		erg = isPalindrom(arr);
		System.out.println(erg);
		
		String s = "anna";
		erg = isPalindrom(s);
		System.out.println(erg);

		s = "rotor";
		erg = isPalindrom(s);
		System.out.println(erg);
}
	
	private static boolean isPalindrom(Character[] arr) {
		
		if (arr == null)
			return false;
		
		Character head;
		Character tail;
		boolean equal = false;
		
		ArrayDeque<Character> deque = new ArrayDeque<>();
		deque.addAll(Arrays.asList(arr));

		while (deque.size() > 1) {
			head = deque.pollFirst();
			tail = deque.pollLast();
			if (head == tail)
				equal = true;
			else
				return false;
		}
		return equal;
	}
	
	private static boolean isPalindrom(String s) {
		
		List<Character> l = new ArrayList<>();
		s.chars().forEach(x -> l.add((char) x));
		
		Character[] arr = new Character[l.size()];
		arr = l.toArray(arr);
			
		return isPalindrom(arr);
	}

}
