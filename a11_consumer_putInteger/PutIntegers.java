package a11_consumer_putInteger;

import java.util.function.*;
import java.util.*;

public class PutIntegers {

	public static void main(String[] args) {

		StringBuilder sb = new StringBuilder();
		putIntegers (i -> sb.append(i).append(" "));
		System.out.println(sb);
		
		List<Integer> list = new ArrayList<>();
		putIntegers(i -> list.add(i));
		System.out.println(list);
		
		putIntegers(System.out::print);
		
	}

	public static void putIntegers(Consumer<Integer> c) {
		for (int i=1; i<=4; i++)
			c.accept(i);
	}
	
}
