package a07_nested_autos;

public class Test {

	public static void main(String[] args) {
			
		Rennwagen rw = new Rennwagen("Mercedes");
		Rennwagen.Fahrer f = new Rennwagen.Fahrer("M. ", "Schuhmacher");
		
		rw.setFahrer(f);
		
		Rennwagen.Motor m = rw.getMotor();
		
		System.out.println(rw);
		System.out.println(m);
	}

}


class Rennwagen {
	
	String name;
	Fahrer f;
	Motor m;
	
	public Rennwagen (String name) {
		this.name = name;
	}
	
	static class Fahrer {
		
		String vorname;
		String nachname;
		
		public Fahrer (String vorname, String nachname) {
			this.vorname = vorname;
			this.nachname = nachname;
		}
		
		

	}
	
	class Motor {
		
		String typ;
		
		public Motor (String typ) {
			this.typ = typ;
		}
		
		public String toString() {
			return "Motor "+typ+ " aus dem Rennwagen "+Rennwagen.this.name;
		}
	}

	public void setFahrer(Fahrer f) {
		this.f = f;
	}
	
	public Motor getMotor() {
		return new Motor("Type1");
	}
	
	public String toString() {
		return "Rennwagen "+ name + ". Fahrer: " + f.vorname+f.nachname;
	}

}