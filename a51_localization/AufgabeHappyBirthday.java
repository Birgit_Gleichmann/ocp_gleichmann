package a51_localization;

import java.time.*;
import java.time.format.*;
import java.time.temporal.*;
import java.util.*;

public class AufgabeHappyBirthday {

	public static void main(String[] args) {
		
		String baseName = "a51_localization.text";
		ResourceBundle bundleText = ResourceBundle.getBundle(baseName);

		String today = bundleText.getString("today");
		String is = bundleText.getString("is");
		String language = bundleText.getString("language");
		Locale.setDefault(new Locale(language));
		
		LocalDate date = LocalDate.now();
		
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd. MMMM yyyy");
		String dateString = dateFormatter.format(date);
		System.out.println(today+dateString+".");
		dateFormatter = DateTimeFormatter.ofPattern("EEEE");
		String weekdayString = dateFormatter.format(date);
		System.out.println(is+weekdayString+".");
		
		System.out.println("*****");
		
		Locale.setDefault(Locale.FRANCE);

		bundleText = ResourceBundle.getBundle(baseName);
		today = bundleText.getString("today");
		is = bundleText.getString("is");
		language = bundleText.getString("language");
		Locale.setDefault(new Locale(language));
		dateFormatter = DateTimeFormatter.ofPattern("dd. MMMM yyyy");
		dateString = dateFormatter.format(date);
		System.out.println(today+dateString+".");
		dateFormatter = DateTimeFormatter.ofPattern("EEEE");
		weekdayString = dateFormatter.format(date);
		System.out.println(is+weekdayString+".");
		
		System.out.println("*****");
		
		bundleText = ResourceBundle.getBundle(baseName);
		String wish = bundleText.getString("wish");
		String tomorrow = bundleText.getString("tomorrow");
		String timeDays = bundleText.getString("timeDays");

		String birthdayAsString = System.getProperty("birthday");
//		String birthdayAsString = "24.02";
		
		if (birthdayAsString != null) {
			DateTimeFormatter parser = DateTimeFormatter.ofPattern("dd.MM");
			TemporalAccessor accessor = parser.parse(birthdayAsString);
			int day = accessor.get(ChronoField.DAY_OF_MONTH);
			int month = accessor.get(ChronoField.MONTH_OF_YEAR);
			
			LocalDate birthday = date.withMonth(month).withDayOfMonth(day);
			
			if (birthday.compareTo(date) < 0) {
				birthday = birthday.withYear(birthday.getYear()+1);
			}
			
			int days = (int)ChronoUnit.DAYS.between(date, birthday);
			
			switch (days) {
			case 0:
				System.out.println(wish);
				break;
			case 1:
				System.out.println(days+" "+tomorrow);
				break;
			default:
				System.out.printf(timeDays+"\n",days);
				break;
			}
			
			System.out.println("*****");
		}
	
	}
}
