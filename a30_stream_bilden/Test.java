package a30_stream_bilden;

import java.nio.file.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.*;

public class Test {

	public static void main(String[] args) {
		a1();
		a2();
		a3();
		a4();
		a5();
	}
	
	public static void a5() {
		//A
//		Collection<Path> coll = new ArrayList<>();
//		coll.add(Paths.get("a/"));
//		coll.add(Paths.get("a/b"));
//		coll.add(Paths.get("a/b/c"));
//		coll.add(Paths.get("a/b/c/d"));
//		
//		for (Path p : coll) {
//			System.out.println(p);
//		}
		//B
		
		Collector<Path, ?, List<Path>> downstream = Collectors.toList();
		Function<String, Path> mapperStringToPath = s -> Paths.get(s);

		Collector<String, ?, List<Path>> mapper 
				= Collectors.mapping(mapperStringToPath, downstream);
		
		List<Path> coll = Stream.of("a", "a/b", "a/b/c", "a/b/c/d").collect(mapper);
		coll.stream().forEach(System.out::println);

		System.out.println("*****");
	}
	
	public static void a4() {
		String[] a1 = {"a", "b"};
		String[] a2 = {"c", "d"};
		String[] a3 = {"e", "f"};
		
		//A
		String[][] a4 = {a1, a2, a3};
		for (String[] arr : a4) {
			for (String s : arr) {
				System.out.println(s);
			}
		}
		//B
		
		System.out.println("*****");
		
		Stream.concat(Stream.concat(Stream.of(a1), Stream.of(a2)), Stream.of(a3))
			.forEach(System.out::println);
		
		System.out.println("*****");
	}
	
	public static void a3() {
		for (int i=100; i>=1; i--) {
			System.out.println(i);
		}

		System.out.println("*****");

		Stream.iterate(100, x -> x-1).limit(100).forEach(System.out::println);

		System.out.println("*****");
}
	
	public static void a2() {
		
		for (int i=0; i<100; i++) {
			System.out.println(nextInt());
		}
		
		System.out.println("*****");
		
		Stream.generate(() -> nextInt())
			.limit(100)
			.forEach(System.out::println);

		System.out.println("*****");
	}
	
	static Integer nextInt() {
		return new Random().nextInt();
	}
	
	
	public static void a1() {

		List<Integer> list1 = Arrays.asList(1, 2, 3);
		List<Integer> list2 = Arrays.asList(55, 77);
		
		// A
		List<List<Integer>> list3 = Arrays.asList(list1, list2);
		for (List<Integer> e : list3) {
			System.out.println("size = " + e.size() + ". elements = " + e);
		}
		// B
		
		System.out.println("*****");
		
		Stream.of(list1, list2)
				.forEach(e -> System.out.println("size = " + e.size() + ". elements = " + e));;

		System.out.println("*****");
	}

}
