package a13_supplier_tracing;

import java.util.function.*;

public class Foo {
	
	MyTracer tracer = new MyTracer(MyTracer.Level.TRACE);

	public void m(String[] args) {
	
		Supplier<String> supplier = () -> "args length: "+args.length;
		tracer.trace(supplier);
	}

}


