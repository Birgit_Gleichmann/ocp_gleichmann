package a13_supplier_tracing;

import java.util.function.*;

public class MyTracer {

	public enum Level {
		NONE, TRACE
	}
	
	private final Level level;
	
	public MyTracer (Level level) {
		this.level = level;
	}
	
	public void trace (Supplier<String> s) {
		String message;
		if (level == Level.TRACE) {
			message = s.get();
			System.out.println(message);
		}
	}
	
}
