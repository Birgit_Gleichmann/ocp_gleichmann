package a05_enums_hunderassen;

public class Hund {

	Hunderasse rasse;
	
	public static void main(String[] args) {
		Hund hund1 = new Hund();
		hund1.rasse = Hunderasse.DACKEL;
		Hund hund2 = new Hund();
		hund2.rasse = Hunderasse.COLLIE;
		Hund hund3 = new Hund();
		hund3.rasse = Hunderasse.DOGGE;
		
		System.out.println(hund1.rasse+", max. Größe: "+hund1.rasse.getMaxGroesse());
		System.out.println(hund2.rasse+", max. Größe: "+hund2.rasse.getMaxGroesse());
		System.out.println(hund3.rasse+", max. Größe: "+hund3.rasse.getMaxGroesse());
	}

}

enum Hunderasse {DACKEL(0.5),
				 COLLIE(1.0),
				 DOGGE(1.5);
	
	Hunderasse(double groesse) {
		this.groesse = groesse;
	}
				 
	private double groesse;
	
	public double getMaxGroesse() {
		return groesse;
	}
}