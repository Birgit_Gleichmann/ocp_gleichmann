package a10_nested_gebaeude;

public class TestGebaeude {

	public static void main(String[] args) {
		
		Gebaeude g = Gebaeude.createGebaeude("Hauptstrasse", 45, 3, 10);
		Gebaeude.Raum r = g.getRaum(0, 2);
		System.out.println("Raum "+r);
		
//		Gebaeude.Raum r2 = g.getRaum(4, 2);
//		System.out.println("Raum "+r2);
//		Gebaeude.Raum r3 = g.getRaum(0, 12);
//		System.out.println("Raum "+r3);
	}

}

class Gebaeude {
	
	String strasse;
	int hausnummer;
	Stockwerk[] stockwerke;
	
	private Gebaeude(String strasse, int hausnummer, int anzStockwerke, int anzRaeume) {
		this.strasse = strasse;
		this.hausnummer = hausnummer;
		stockwerke = new Stockwerk[anzStockwerke];
		for (int i=0; i<anzStockwerke; i++) {
			stockwerke[i] = new Stockwerk(i, anzRaeume);
		}
	}
	
	public static Gebaeude createGebaeude(String strasse, int hausnummer, int anzStockwerke, int anzRaeume) {
		
		Gebaeude g = null;
		if (anzStockwerke > 0 && anzRaeume > 0) {
			g = new Gebaeude(strasse, hausnummer, anzStockwerke, anzRaeume);
		} else {
			System.out.println("Gebaeudedaten ungültig !");
		}
		return g;
	}
	
	public Stockwerk getStockwerk(int stockwerk) {
		if (stockwerk > 0 && stockwerk < stockwerke.length) {
			return stockwerke[stockwerk];
		} else {
			System.out.println("Stockwerk nicht vorhanden !");
			return null;
		}
	}
	
	public Raum getRaum(int stockwerkNr, int raumNr) {
		Raum r = null;
		try {
			r = stockwerke[stockwerkNr].raeume[raumNr];
		} catch (Exception e) {
			System.out.println("Raum nicht vorhanden !");
		}
		return r;
	}
	
	
	
	class Stockwerk {
		
		int stockwerkNr;
		Raum[] raeume = null;
		
		public Stockwerk (int stockwerkNr, int anzRaeume) {
			this.stockwerkNr = stockwerkNr;
			raeume = new Raum[anzRaeume];
			for (int i=0; i<anzRaeume; i++) {
				raeume[i] = new Raum(stockwerkNr, i);
			}
		}
	}
	
	class Raum {
		
		int stockwerkNr;
		int raumNr;
		
		public Raum(int stockwerkNr, int raumNr) {
			this.stockwerkNr = stockwerkNr;
			this.raumNr = raumNr;
		}
		
		@Override
		public String toString() {
			return "Raum "+stockwerkNr+"."+raumNr+" / "+strasse+" "+hausnummer;
		}
		
		
	}
}
