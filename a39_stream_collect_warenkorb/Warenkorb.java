package a39_stream_collect_warenkorb;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class Warenkorb {

	public static void main(String[] args) {
		List<Produkt> warenkorb = new ArrayList<>();
		warenkorb.add(new Produkt("Brot", 129));
		warenkorb.add(new Produkt("Wurst", 230));
		warenkorb.add(new Produkt("Milch", 99));
		warenkorb.add(new Produkt("Milch", 99));
		
		calculatePrice(warenkorb);
		
		System.out.println("*****");
		
		// A2
		List<Bestellung> bestellungen = new ArrayList<>();
		bestellungen.add(new Bestellung("Brot", 3));
		bestellungen.add(new Bestellung("Wurst", 1));
		bestellungen.add(new Bestellung("Milch", 2));
		
		List<Produkt> warenkorb2 = buildWarenkorb(bestellungen);
		warenkorb2.forEach(System.out::println);
		
		System.out.println("*****");
		
		calculatePrice(warenkorb2);
	}
	
	static void calculatePrice(List<Produkt> warenkorb) {

		// A1
		ToIntFunction<Produkt> mapper = x -> x.getPreis();
		Collector<Produkt, ?, Integer> collector = Collectors.summingInt(mapper);
		int summe = warenkorb.stream().collect(collector);
		System.out.println("Summe: "+summe);
	}
	
	static List<Produkt> buildWarenkorb(List<Bestellung> bestellungen) {
		
		Map<String, Integer> preisMap = new HashMap<>();
		preisMap.put("Brot", 129);
		preisMap.put("Wurst", 230);
		preisMap.put("Milch", 99);
		
		Function<Bestellung, Stream<Produkt>> mapper1 = 
				x -> Stream.generate(() -> new Produkt(x.getProduktName(), preisMap.get(x.getProduktName()))).limit(x.getAnzahl());
		Collector <Produkt, ?, ArrayList<Produkt>> collector = Collectors.toCollection(ArrayList::new);
		List<Produkt> produkte = bestellungen.stream().flatMap(mapper1 ).collect(collector);
		
		return produkte;
	}
}

class Produkt {
	private String name;
	private int preis;
	
	public Produkt (String name, int preis) {
		this.name = name;
		this.preis = preis;
	}
	
	public int getPreis() {
		return preis;
	}
	
	@Override
	public String toString() {
		return name+", "+preis;
	}
}

class Bestellung {
	private String produktName;
	private int anzahl; // gewünschte Anzahl der Produkt-Objekte
	
	public Bestellung(String produktName, int anzahl) {
		this.produktName = produktName;
		this.anzahl = anzahl;
	}
	
	public String getProduktName() {
		return produktName;
	}
	
	public int getAnzahl() {
		return anzahl;
	}
}
