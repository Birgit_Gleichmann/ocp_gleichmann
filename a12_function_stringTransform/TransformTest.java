package a12_function_stringTransform;

import java.util.function.*;

public class TransformTest {

	public static void main(String[] args) {
		
		
		StringTransform t1 = new StringTransform()
				.addTransformation( s -> s.toUpperCase() )
				.addTransformation( s -> s + "!" );
		
		String s1 = t1.process("Hallo");
		System.out.println(s1);
		
		s1 = t1.process("Java ist toll");
		System.out.println(s1);
		
		StringTransform t2 = new StringTransform()
				.addTransformation( s -> s + " Welt")
				.addTransformation( s -> s.toLowerCase())
				.addTransformation( s -> s + " !");
		
		s1 = t2.process("Hallo");
		System.out.println(s1);
	}

}

class StringTransform {
	
	Function<String, String> function = s -> s;
	
	public StringTransform addTransformation (Function<String, String> function) {
		this.function = this.function.andThen(function);
		return this;
	}
	
	public String process(String s) {
		String t = function.apply(s);
		return t;
	}
	
}
