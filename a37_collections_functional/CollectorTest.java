package a37_collections_functional;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class CollectorTest {

	public static void main(String[] args) {
		
		// A1
		List<Integer> list1 = createList();
		list1.forEach(System.out::println);
		
		System.out.println("*****");
		
		// A2
		List<Integer> list2 = createList();
		list2.removeIf(x -> x%2 != 0);
		list2.forEach(System.out::println);
		
		System.out.println("*****");
		
		// A3
		List<Integer> list3 = createList();
		UnaryOperator<Integer> operator = x -> {if (x%2 == 0) return x;
												else return 0;};
		list3.replaceAll(operator);
		list3.forEach(System.out::println);

		System.out.println("*****");
		
		// A4-1
		List<Integer> list4_1 = createList();
		list4_1.sort( (x, y) -> y.compareTo(x));
		list4_1.forEach(System.out::println);

		System.out.println("*****");
		
		// A4-2
		List<Integer> list4_2 = createList();
		list4_2.sort(new Comparator<Integer>() {@Override
		public int compare(Integer o1, Integer o2) {
			return o2.compareTo(o1);
		} });
		list4_2.forEach(System.out::println);

		System.out.println("*****");
		
		// A4-3
		List<Integer> list4_3 = createList();
		list4_3.sort(Comparator.reverseOrder());
		list4_3.forEach(System.out::println);

		System.out.println("*****");
		
		// A5
		List<Integer> list5 = new ArrayList<Integer>(Arrays.asList(null,2,3,null,5,6,7,8));
		System.out.println(list5.stream().reduce(0,(x,y) -> {if (x == null) return 0; 
															else if (y!= null) return (x+y);
															else return x;}));
	}	
		
	static ArrayList<Integer> createList() {	
		return new ArrayList<Integer>(Arrays.asList(1,2,3,4,5,6,7,8));
	}
}
