package a38_stream_collect_collectors_personen;

import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class PersonenTest {

	public static void main(String[] args) {
		
		Person[] personen = {
				new Person("Tom", "Bauarbeiter(in)"),
				new Person("Jerry", "Lehrer(in)"),
				new Person("Peter", "Metzger(in)"),
				new Person("Paul", "Bauarbeiter(in)"),
				new Person("Mary", "Lehrer(in)")
		};
		
		
		Collector <Person, ?, TreeSet<Person>> collector = Collectors.toCollection(TreeSet::new);
		TreeSet<Person> list1 = Arrays.stream(personen).collect(collector);
		
		list1.forEach(System.out::println);
	
		System.out.println("*****");
		
		Function<Person, String> classifier = s -> s.getBeruf();
		Collector<Person, ?, Map<String, List<Person>>> collector2 = Collectors.groupingBy(classifier);
		Map<String, List<Person>> list2 = Arrays.stream(personen).collect(collector2);
		
		BiConsumer<? super String, ? super List<Person>> action = (x,y) -> System.out.println(x + " = " + y);
		list2.forEach(action);

		System.out.println("*****");
		
		Function<Person, String> mapper = x -> x.getBeruf();
		Collector<String, ?, TreeSet<String>> downstream = Collectors.toCollection(TreeSet::new);
		Collector<Person, ?, TreeSet<String>> collector3 = Collectors.mapping(mapper, downstream);
		TreeSet<String> list3 = Arrays.stream(personen).collect(collector3);
		
		list3.forEach(System.out::println);
		
	}

}

class Person implements Comparable<Person>{

	private String name;
	private String beruf;
	
	public Person(String name, String beruf) {
		this.name = name;
		this.beruf = beruf;
	}
	
	public String getBeruf() {
		return beruf;
	}
	
	public int compareTo(Person o2) {
		return name.compareTo(o2.name);
	}
	
	public String toString() {
		return name;
	}
}
