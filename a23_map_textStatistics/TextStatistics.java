package a23_map_textStatistics;

import java.util.*;

public class TextStatistics {
	
	static String str;
	static Set<Character> charSet;
	

	public static void main(String[] args) {
		TextStatistics stat = TextStatistics.of("Heute ist Montag!");
		Collection<Character> chars = stat.getUniqueChars();
		System.out.println(chars);
		
		Map<Character, Integer> charMap = TextStatistics.getCharCounts();
		System.out.println(charMap);
	}

	static TextStatistics of (String s) {
		str = s;
		return new TextStatistics();
	}
	
	static Collection<Character> getUniqueChars() {
		charSet = new TreeSet<Character>();
		for (int i=0; i<str.length(); i++) {
			charSet.add(str.charAt(i));
		}
		return charSet;
	}
	
	static Map<Character, Integer> getCharCounts() {
		Map<Character, Integer> map = new TreeMap<>();
		for (int i=0; i<str.length(); i++) {
			Character x = str.charAt(i);
			Integer counter = map.get(x);
			if (counter == null) {
				counter = 0;
			}
			counter ++;
			map.put(x, counter);
		}		
		return map;
	}
}
